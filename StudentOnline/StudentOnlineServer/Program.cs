﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace StudentOnlineServer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            TCPConnection tcpC = new TCPConnection();
            tcpC.TCPListen();
        }
    }
}