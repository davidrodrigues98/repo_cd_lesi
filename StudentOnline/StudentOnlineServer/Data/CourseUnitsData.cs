﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;

namespace StudentOnlineServer
{
    class CourseUnitsData
    {
        // We are creating 5 course units, where one course can have, at least, one course unit registered, and there is at leat 2 course units with more than one course.
        // This will enable the possibility of grouping users from distict courses in the same chat lobby.
        private static List<CourseUnit> courseunits = new List<CourseUnit>
        {
            new CourseUnit {
                Name = "Comunicação de Dados",
                Id = 0,
                JoinedCourses = new List<int> { 0 }
            },
            new CourseUnit {
                Name = "Jogos Digitais",
                Id = 1,
                JoinedCourses = new List<int> { 2 }
            },
            new CourseUnit {
                Name = "Máquinas de Tempo Real",
                Id = 2,
                JoinedCourses = new List<int> { 1 }
            },
            new CourseUnit {
                Name = "Fundamentos de Física",
                Id = 3,
                JoinedCourses = new List<int> { 0, 1, 2 }
            },
            new CourseUnit {
                Name = "Métodos Numéricos",
                Id = 4,
                JoinedCourses = new List<int> { 0, 1 }
            }
        };

        /// <summary>
        /// Data mockup with 5 course units. This courses are all shuffled by the available courses.
        /// </summary>
        public static List<CourseUnit> AvailableCourseUnits { get => courseunits; }
    }
}
