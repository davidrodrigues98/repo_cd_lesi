﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;
using System.Collections.ObjectModel;

namespace StudentOnlineServer
{
    public static class LobbiesData
    {
        private static List<Lobby> openRooms = new List<Lobby>();

        private static List<ChatMessage> roomMessages = new List<ChatMessage>();

        public static Lobby CreateRoom(User userWhoRequested)
        {
            Lobby newLobby = new Lobby
            {
                AssociatedCourseUnitId = userWhoRequested.CourseUnitId,
                AssociativeName = "Room " + openRooms.Count.ToString(),
                MemberCount = 0
            };
            openRooms.Add(newLobby);
            
            return newLobby;
        }

        public static void IncreaseChatMemberCount(Lobby dest)
        {
            openRooms.Where(x => x.AssociativeName == dest.AssociativeName).First().MemberCount++;
        }
    }
}
