﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentOnline.BusinessObject;

namespace StudentOnlineServer
{
    public static class UsersData
    {
        private static List<User> userList = new List<User>
        {
            new User {
                CourseId = 0,
                CourseUnitId = 3,
                Emailaddress = "a14872@alunos.ipca.pt",
                Name = "David Rodrigues",
                Password = "a14872",
                RoleId = 1
            },
            new User {
                CourseId = 0,
                CourseUnitId = 1,
                Emailaddress = "a14870@alunos.ipca.pt",
                Name = "Artur Mendes",
                Password = "a14870",
                RoleId = 1
            },
            new User {
                CourseId = 0,
                CourseUnitId = 1,
                Emailaddress = "a17023@alunos.ipca.pt",
                Name = "Helder Sá",
                Password = "a17023",
                RoleId = 1
            },
            new User {
                CourseId = 0,
                CourseUnitId = 3,
                Emailaddress = "a17005@alunos.ipca.pt",
                Name = "Rui Costa",
                Password = "a17005",
                RoleId = 1
            },
            new User
            {
                CourseId = 0,
                CourseUnitId = 3,
                Emailaddress = "professorteste@ipca.pt",
                Name = "Professor Teste",
                Password = "professorteste",
                RoleId = 0
            }
        };

        /// <summary>
        /// Mockup data structure with some user account examples.
        /// </summary>
        public static List<User> UserList { get => userList; }

        public static int AddUser(User user)
        {
            if (userList.Where(x => x.Emailaddress == user.Emailaddress).Count() > 0)
                return 101;
            else
                userList.Add(user);
            return 100;
        }

        public static object LoginSessionStart(LoginCredSender loginInfo)
        {
            string userEmail = loginInfo.UserEmail;
            string userPassword = loginInfo.UserPassword;
            if (userList.Where(x => x.Emailaddress == userEmail && x.Password == userPassword).Count() == 1)
            {
                //add gui to the active sessions and returns gui to the client
                Guid newSessionGuid = Guid.NewGuid();
                bool isAdmin;
                if (userList.Where(x => x.Emailaddress == userEmail && x.Password == userPassword).First().RoleId == 0)
                    isAdmin = true;
                else
                    isAdmin = false;

                GUIDSender sendGui = new GUIDSender
                {
                    GuidAddress = newSessionGuid,
                    IsAdmin = isAdmin
                };
                Console.WriteLine(newSessionGuid.ToString());
                TCPConnection.AddActiveSession(userList.Where(x => x.Emailaddress == userEmail && x.Password == userPassword).First(), newSessionGuid);
                return sendGui;
                
            }
            else
                return -2;
        }
    }
}
