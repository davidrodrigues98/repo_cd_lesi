﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;

namespace StudentOnlineServer
{
    class RolesData
    {
        private static List<Role> allowedroles = new List<Role> {
            new Role { Name = "Teacher", Id = 0 },
            new Role { Name = "Student", Id = 1 }
        };

        /// <summary>
        /// Data mockup with the 2 main roles we could possibly use in this case scenario.
        /// </summary>
        public static List<Role> Allowedroles { get => allowedroles; }
    }
}
