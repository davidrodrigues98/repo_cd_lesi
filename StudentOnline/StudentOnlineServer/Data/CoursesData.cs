﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;

namespace StudentOnlineServer
{
    class CoursesData
    {
        private static List<Course> courses = new List<Course>
        {
            new Course { Name = "Engenharia de Sistemas de Informação", Id = 0 },
            new Course { Name = "Engenharia de Informática Médica", Id = 1 },
            new Course { Name = "Engenharia de Desenvolvimento de Jogos Digitais", Id = 2 }
        };

        /// <summary>
        /// Data mockup with examples of courses.
        /// </summary>
        public static List<Course> AvailableCourses { get => courses; }
    }
}
