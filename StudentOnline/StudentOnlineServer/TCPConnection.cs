﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using StudentOnline;
using StudentOnline.BusinessObject;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace StudentOnlineServer
{
    /// <summary>
    /// This will constantly response client requests on a separate thread, while the main thread process input commands.
    /// </summary>
    public class TCPConnection
    {
        // We will use both dictionaries to help the server linking users with TCP easier (no need to use LINQ to fetch simple data requests).
        private static Dictionary<Guid, User> activeSessionsUsers = new Dictionary<Guid, User>();
        private static Dictionary<Guid, TcpClient> activeClientSessions = new Dictionary<Guid, TcpClient>();
        private static Dictionary<Guid, Lobby> activeUsersConnectedToChats = new Dictionary<Guid, Lobby>();

        private static IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 9000);
        private static TcpListener tcpListener = new TcpListener(ipEndPoint);

        /// <summary>
        /// Server starts listening when it starts.
        /// </summary>
        public TCPConnection()
        {
            tcpListener.Start();
        }

        /// <summary>
        /// This is the main function for socket management and communication. Changing this will not affect any classes inside the application.
        /// </summary>
        public void TCPListen()
        {
            while (true)
            {
                TcpClient tcpClient = tcpListener.AcceptTcpClient();
                Console.WriteLine("Started a new connection.");

                var networkStream = tcpClient.GetStream();

                if(networkStream.CanRead)
                {
                    StudentOnlineObject receivedObject = (new BinaryFormatter()).Deserialize(networkStream) as StudentOnlineObject;
                    Console.WriteLine("Received object of type: " + receivedObject.ObjectType.ToString());
                    object returnData = ObjectProcess(receivedObject);

                    if (networkStream.CanWrite)
                    {
                        (new BinaryFormatter()).Serialize(networkStream, returnData);
                    }

                    // When the server finds a Guid, it keeps the client opened and stores it with the another active session clients. This will grande continuous communication between client and server.
                    if (returnData is GUIDSender)
                        AddActiveSession(tcpClient, (returnData as GUIDSender).GuidAddress);
                    else
                        tcpClient.Close();
                }

                
            }
        }

        #region Session management
        /// <summary>
        /// This method adds a socket to the sessions dictionary, this will keep permanent tracking of clients who will use the chatting system.
        /// </summary>
        public static void AddActiveSession(TcpClient client, Guid sessionIdentifier)
        {
            Thread thread = new Thread(new ParameterizedThreadStart(ClientHandler));
            thread.IsBackground = true;
            thread.Start(new object[] { client, sessionIdentifier });
        }
        public static void AddActiveSession(User relatedUser, Guid sessionIdentifier)
        {
            activeSessionsUsers.Add(sessionIdentifier, relatedUser);
        }


        public static void ClientHandler(object data)
        {
            TcpClient client = (TcpClient)(data as object[])[0];
            Guid sessionIdentifier = (Guid)(data as object[])[1];
            activeClientSessions.Add(sessionIdentifier, client);

            NetworkStream networkStream = client.GetStream();
            User sessionUser = activeSessionsUsers[sessionIdentifier];

            bool connected = true;
            while (connected)
            {
                Thread.Sleep(100);
                //try
                //{
                object returner = null;
                    if (networkStream.DataAvailable)
                    {
                        Console.WriteLine("\tAwaiting session request from " + sessionUser.Name);
                        StudentOnlineObject receivedData = (new BinaryFormatter()).Deserialize(networkStream) as StudentOnlineObject;
                            returner = ObjectProcess(receivedData);
                    }

                    if (networkStream.CanWrite && returner != null)
                    {
                        (new BinaryFormatter()).Serialize(networkStream, returner);
                    }
                //}
                //catch (Exception e)
                //{
                //Console.WriteLine(e.Message);
                //connected = false;
                //Console.WriteLine("Lost connection with " + sessionUser.Name);
                //}
            }
        }
        #endregion

        #region Processing requests

        /// <summary>
        /// This provides all server business data to the client. This is a way of maintaining everyone updated.
        /// </summary>
        /// <returns>Returns a dictionary which contains all non-sensitive data structures.</returns>
        static Dictionary<string, object> ServerDataBundle()
        {
            return new Dictionary<string, object>
            {
                { "Roles", RolesData.Allowedroles },
                { "Courses", CoursesData.AvailableCourses },
                { "CourseUnits", CourseUnitsData.AvailableCourseUnits }
            };
        }

        /// <summary>
        /// This process requests by their type. The return can be a code or an object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        static object ObjectProcess(StudentOnlineObject obj)
        {
            object returner = null;
            switch(obj.ObjectType)
            {
                case StudentOnlineObject.Type.REGISTRATION:
                    returner = UsersData.AddUser(obj as User);
                    break;
                case StudentOnlineObject.Type.CLIENTOPEN:
                    returner = ServerDataBundle();
                    break;
                case StudentOnlineObject.Type.LOGINCREDENTIALS:
                    returner = UsersData.LoginSessionStart(obj as LoginCredSender);
                    break;
                case StudentOnlineObject.Type.ROOMADDING:
                    Lobby newRoom = LobbiesData.CreateRoom(activeSessionsUsers[(obj as GUIDSender).GuidAddress]);
                    NotifyRoomCreated(newRoom);
                    break;
                case StudentOnlineObject.Type.MESSAGESENDING:
                    ChatMessage join = (obj as ChatMessage);
                    activeUsersConnectedToChats.Add(join.UserGuid, join.ConnectedLobby);
                    LobbiesData.IncreaseChatMemberCount(join.ConnectedLobby);
                    NotifyUserJoinedChat(join);
                    break;
                case StudentOnlineObject.Type.TEXTSENDING:
                    ChatMessage text = (obj as ChatMessage);
                    SendTextMessageToUsers(text);
                    break;
                default:
                    returner = -3;
                    break;
            }
            return returner;
        }
        #endregion

        static void SendTextMessageToUsers(ChatMessage textMessage)
        {
            Lobby inLobby = textMessage.ConnectedLobby;
            var activeUsersFiltered = activeUsersConnectedToChats.Where(x => x.Value.AssociativeName == inLobby.AssociativeName);
            string userName = activeSessionsUsers[textMessage.UserGuid].Name;
            textMessage.UserName = userName;
            foreach (var key in activeUsersFiltered)
            {
                TcpClient thisTCP = activeClientSessions[key.Key];
                NetworkStream networkStream = thisTCP.GetStream();
                textMessage.ObjectType = StudentOnlineObject.Type.TEXTSENT;
                bool done = false;
                while (done == false)
                {
                    if (networkStream.CanWrite)
                    {
                        (new BinaryFormatter()).Serialize(networkStream, textMessage);
                        done = true;
                    }
                }
            }
        }

        static void NotifyRoomCreated(Lobby room)
        {
            var activeUsersFiltered = activeSessionsUsers.Where(x => x.Value.CourseUnitId == room.AssociatedCourseUnitId);
            foreach(var key in activeUsersFiltered)
            {
                TcpClient thisTCP = activeClientSessions[key.Key];
                NetworkStream networkStream = thisTCP.GetStream();
                room.ObjectType = StudentOnlineObject.Type.ROOMADDED;
                bool done = false;
                while (done == false)
                {
                    if (networkStream.CanWrite)
                    {
                        Console.WriteLine("\tNotifying clients.");
                        (new BinaryFormatter()).Serialize(networkStream, room);
                        done = true;
                    }
                }
            }
        }

        static void NotifyUserJoinedChat(ChatMessage notification)
        {
            
            Lobby inLobby = notification.ConnectedLobby;
            var activeUsersFiltered = activeUsersConnectedToChats.Where(x => x.Value.AssociativeName == inLobby.AssociativeName);
            string userName = activeSessionsUsers[notification.UserGuid].Name;
            notification.UserName = userName;
            notification.Text = userName + " joined the chat!";
            foreach(var key in activeUsersFiltered)
            {
                TcpClient thisTCP = activeClientSessions[key.Key];
                NetworkStream networkStream = thisTCP.GetStream();
                notification.ObjectType = StudentOnlineObject.Type.MESSAGESENT;
                bool done = false;
                while(done == false)
                {
                    if (networkStream.CanWrite)
                    {
                        (new BinaryFormatter()).Serialize(networkStream, notification);
                        done = true;
                    }
                }
            }
        }

    }
}
