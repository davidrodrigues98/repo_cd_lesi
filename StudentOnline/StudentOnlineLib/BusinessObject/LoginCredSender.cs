﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline;

namespace StudentOnline.BusinessObject
{
    [SerializableAttribute]
    public class LoginCredSender : StudentOnlineObject
    {
        private string userEmail;
        private string userPassword;

        public LoginCredSender(string email, string password)
        {
            UserEmail = email;
            UserPassword = password;
        }

        /// <summary>
        /// User inserted email.
        /// </summary>
        public string UserEmail { get => userEmail; set => userEmail = value; }

        /// <summary>
        /// User inserted password. Note that this is a visible string. We are not Encoding it because it is not necessary for this scenario.
        /// </summary>
        public string UserPassword { get => userPassword; set => userPassword = value; }
    }
}
