﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentOnline.BusinessObject
{
    /// <summary>
    /// A representative role class for the application users. Roles tags each user with a certain access level to some of the application functionalities.
    /// </summary>
    [SerializableAttribute]
    public class Role : StudentOnlineObject
    {
        private string name;
        private int id;

        #region Properties

        //The role's name to be applied.
        public string Name { get => name; set => name = value; }

        //The identifier of the role. Useful if we need to change role's name later on, or to improve database relations (if required in the future).
        public int Id { get => id; set => id = value; }

        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}
