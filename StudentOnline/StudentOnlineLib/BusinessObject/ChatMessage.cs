﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentOnline.BusinessObject
{
    /// <summary>
    /// Represents all chat text components.
    /// </summary>
    [SerializableAttribute]
    public class ChatMessage : StudentOnlineObject
    {
        /// <summary>
        /// Defines if a message is an user text message or a notification.
        /// </summary>
        public enum MessageType
        {
            TEXTMESSAGE,    // A default text message.
            NOTIFICATION    // Notification from user or server.
        }

        public MessageType messageType;

        private Guid userGuid;
        private Lobby connectedLobby;
        private string text;
        private string userName = "System";

        /// <summary>
        /// The connection with the user who sent the message.
        /// </summary>
        public Guid UserGuid { get => userGuid; set => userGuid = value; }

        /// <summary>
        /// The lobby object for easier comparison.
        /// </summary>
        public Lobby ConnectedLobby { get => connectedLobby; set => connectedLobby = value; }

        /// <summary>
        /// The message content.
        /// </summary>
        public string Text { get => text; set => text = value; }
        public string UserName { get => userName; set => userName = value; }
    }
}
