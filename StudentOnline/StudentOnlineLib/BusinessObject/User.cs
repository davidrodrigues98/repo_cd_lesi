﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentOnline.BusinessObject
{
    [SerializableAttribute]
    public class User : StudentOnlineObject
    {
        private string name;
        private string emailaddress;
        private int roleId;
        private int courseId;
        private int courseUnitId;
        private string password;

        /// <summary>
        /// User's first and last names.
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// User's email address.
        /// </summary>
        public string Emailaddress { get => emailaddress; set => emailaddress = value; }

        /// <summary>
        /// User's Role Id.
        /// </summary>
        public int RoleId { get => roleId; set => roleId = value; }

        /// <summary>
        /// User's Course Id.
        /// </summary>
        public int CourseId { get => courseId; set => courseId = value; }

        /// <summary>
        /// User's Course Unit Id.
        /// </summary>
        public int CourseUnitId { get => courseUnitId; set => courseUnitId = value; }

        /// <summary>
        /// This will only be used by server and it is not encoded.
        /// </summary>
        public string Password { get => password; set => password = value; }
    }
}
