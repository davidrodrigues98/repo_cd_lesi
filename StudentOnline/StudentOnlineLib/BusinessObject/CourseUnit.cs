﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentOnline.BusinessObject
{
    /// <summary>
    /// Representative business object class for a course unit class. 
    /// </summary>
    [SerializableAttribute]
    public class CourseUnit : StudentOnlineObject
    {
        private string name;
        private int id;
        private List<int> joinedcourses;

        #region Properties

        //The course unit's name to be applied.
        public string Name { get => name; set => name = value; }

        //The identifier of the course unit. Useful if we need to change course's name later on, or to improve database relations (if required in the future).
        public int Id { get => id; set => id = value; }

        //The courses where this CU will appear.
        public List<int> JoinedCourses { get => joinedcourses; set => joinedcourses = value; }

        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}
