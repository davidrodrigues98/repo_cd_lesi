﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentOnline.BusinessObject
{
    /// <summary>
    /// This class is the universal business object class we managed to create. You may understand this as an object header when we need to serialize several data and send it by NetworkStreaming.
    /// Note that every business object can be passed as a StudentOnlineObject.
    /// </summary>
    [SerializableAttribute]
    public class StudentOnlineObject
    {
        /// <summary>
        /// An enumerator which describes the utility of the income package. Server knows what to do with this object when he analyses this enumerator.
        /// </summary>
        public enum Type
        {
            CLIENTOPEN,         // Application start.
            REGISTRATION,       // Registration attempt.
            LOGINCREDENTIALS,   // Login attempt.
            ROOMADDING,         // Asking to add a room.
            ROOMADDED,          // Notify room added.
            MESSAGESENDING,     // Asking to join a room.
            MESSAGESENT,        // Notify room connected.
            TEXTSENDING,        // Asking to send text message.
            TEXTSENT            // Text message sent.
        }

        /// <summary>
        /// The current object type.
        /// </summary>
        public Type ObjectType { get; set; }
    }
}
