﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline;

namespace StudentOnline.BusinessObject
{
    /// <summary>
    /// When the client needs server auth, he needs to send GUI as an approval.
    /// </summary>
    [SerializableAttribute]
    public class GUIDSender : StudentOnlineObject
    {
        private Guid guidAddress;
        private bool isAdmin;

        /// <summary>
        /// Guid Address being sent to the server.
        /// </summary>
        public Guid GuidAddress { get => guidAddress; set => guidAddress = value; }
        /// <summary>
        /// Used by server response to say the client this is an admin.
        /// </summary>
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
    }
}
