﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline;

namespace StudentOnline.BusinessObject
{
    [SerializableAttribute]
    public class Lobby : StudentOnlineObject
    {
        private int associatedCourseUnitId;
        private string associativeName;
        private int memberCount = 0;

        /// <summary>
        /// The course unit this lobby is related to.
        /// </summary>
        public int AssociatedCourseUnitId { get => associatedCourseUnitId; set => associatedCourseUnitId = value; }

        /// <summary>
        /// The name is auto generated when created.
        /// </summary>
        public string AssociativeName { get => associativeName; set => associativeName = value; }

        /// <summary>
        /// Member number to appear on the lobby User Control icon.
        /// </summary>
        public int MemberCount { get => memberCount; set => memberCount = value; }

    }
}
