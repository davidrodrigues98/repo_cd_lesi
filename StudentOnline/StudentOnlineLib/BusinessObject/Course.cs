﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentOnline.BusinessObject
{
    /// <summary>
    /// Representative business object class for a course class. We can later on fill this with more details and RL content.
    /// </summary>
    [SerializableAttribute]
    public class Course : StudentOnlineObject
    {
        private string name;
        private int id;

        #region Properties

        //The course's name to be applied.
        public string Name { get => name; set => name = value; }

        //The identifier of the course. Useful if we need to change course's name later on, or to improve database relations (if required in the future).
        public int Id { get => id; set => id = value; }

        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}
