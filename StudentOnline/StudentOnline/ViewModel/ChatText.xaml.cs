﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StudentOnline.BusinessObject;

namespace StudentOnline.ViewModel
{
    /// <summary>
    /// Interaction logic for ChatMessage.xaml
    /// </summary>
    public partial class ChatText : UserControl
    {
        public ChatText()
        {
            InitializeComponent();
        }
        public ChatText(ChatMessage linkedMessage)
        {
            InitializeComponent();
            labelText.Content = linkedMessage.Text;
            labelUserName.Content = linkedMessage.UserName;
        }
    }
}
