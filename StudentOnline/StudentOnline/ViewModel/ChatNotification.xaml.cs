﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StudentOnline.BusinessObject;

namespace StudentOnline.ViewModel
{
    /// <summary>
    /// Interaction logic for ChatNotification.xaml
    /// </summary>
    public partial class ChatNotification : UserControl
    {
        public ChatNotification()
        {
            InitializeComponent();
        }
        public ChatNotification(ChatMessage linkedMessage)
        {
            InitializeComponent();
            chatnotification_labelNotification.Content = linkedMessage.Text;
        }
    }
}
