﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StudentOnline.BusinessObject;
using StudentOnline.Model;

namespace StudentOnline.ViewModel
{
    /// <summary>
    /// Interaction logic for ChatSelect.xaml
    /// </summary>
    public partial class ChatSelect : UserControl
    {
        private int memberCount = 0;
        private Lobby linkedLobby;

        public int MemberCount { get; set; }
        public Lobby LinkedLobby { get => linkedLobby; set => linkedLobby = value; }

        public ChatSelect()
        {
            InitializeComponent();
        }

        public ChatSelect(Lobby linkedLobby)
        {
            InitializeComponent();
            Width = 100;
            Height = 100;
            LinkedLobby = linkedLobby;
            chatselect_labelCount.Content = linkedLobby.AssociativeName;
            chatselect_labelMemberCount.Content = MemberCount + "Members";
        }

    }
}
