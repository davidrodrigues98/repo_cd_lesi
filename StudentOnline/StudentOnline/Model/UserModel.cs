﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;
using System.Collections.ObjectModel;
using System.Runtime.Serialization.Formatters.Binary;

namespace StudentOnline.Model
{
    /// <summary>
    /// An user model class for our application, responsible for managing connections bewteen client and server when it concerns Users and User related data (courses, classes, etc.).
    /// </summary>
    public static class UserModel
    {
        #region Dinamic structures
        /// <summary>
        /// A data structure ready to receive all the allowed Roles.
        /// </summary>
        private static List<Role> allowedroles;

        /// <summary>
        /// A data structure ready to receive all available courses.
        /// </summary>
        private static List<Course> courses;

        /// <summary>
        /// A data structure ready to receive all available course units.
        /// </summary>
        private static List<CourseUnit> courseunits;

        #endregion

        #region Observable Collections for ViewModel and real time "on change" combobox refresh.

        /// <summary>
        /// Observable collection for bindings with the available roles which must be used.
        /// </summary>
        public static ObservableCollection<Role> AvailableRoles => new ObservableCollection<Role>(allowedroles);

        /// <summary>
        /// Observable collection for bindings with the available courses which must be used.
        /// </summary>
        public static ObservableCollection<Course> AvailableCourses => new ObservableCollection<Course>(courses);

        /// <summary>
        /// Observable collection for bindings with the available course units which must be used.
        /// </summary>
        public static ObservableCollection<CourseUnit> AvailableCourseUnits => new ObservableCollection<CourseUnit>(courseunits);

        #endregion

        #region Server Connections

        /// <summary>
        /// This function load information from server that is considered necessary at the application start up.
        /// Example: Registration combobox filters.
        /// </summary>
        /// <returns></returns>
        public static int StartData()
        {
            StudentOnlineObject sod = new StudentOnlineObject { ObjectType = StudentOnlineObject.Type.CLIENTOPEN };
            //TCPMessage message = TCPConnection.Serialize(sod);
            try
            {
                Dictionary<string, object> bundle = TCPConnection.SendMessage(sod) as Dictionary<string, object>;
                allowedroles = bundle["Roles"] as List<Role>;
                courses = bundle["Courses"] as List<Course>;
                courseunits = bundle["CourseUnits"] as List<CourseUnit>;
                return 0;
            }
            catch
            {
                return -1;
            }

        }

        /// <summary>
        /// Creates TCP connection with the server so the new user data can be sent. Client waits for server response.
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        public static int SendRegistrationData(User newUser)
        {
            newUser.ObjectType = StudentOnlineObject.Type.REGISTRATION;
            try
            {
                //TCPMessage message = TCPConnection.Serialize(newUser);
                return (int)TCPConnection.SendMessage(newUser);
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// Creates TCP Connection with the server and tries to create a session.
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <returns></returns>
        public static object SendLoginSession(LoginCredSender loginInfo)
        {
            loginInfo.ObjectType = StudentOnlineObject.Type.LOGINCREDENTIALS;
            try
            {
                return TCPConnection.SendMessage(loginInfo);
            }
            catch
            {
                return -2;
            }
        }
        #endregion
    }
}
