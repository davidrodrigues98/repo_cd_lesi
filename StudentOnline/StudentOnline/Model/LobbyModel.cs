﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using StudentOnline;

namespace StudentOnline.Model
{
    /// <summary>
    /// This model class will manage client connections with the server when it concerns chat management and permission.
    /// </summary>
    public static class LobbyModel
    {
        private static ObservableCollection<Lobby> localLobbies = new ObservableCollection<Lobby>();
        private static ObservableCollection<ChatMessage> localChatMessages = new ObservableCollection<ChatMessage>();

        /// <summary>
        /// With this obervable collection, the interface knows when to generate new user controls for the lobbbies.
        /// </summary>
        public static ObservableCollection<Lobby> LocalLobbies { get => localLobbies; set => localLobbies = value; }
        /// <summary>
        /// With this obervable collection, the interface knows when to generate new user controls for the chat messages.
        /// </summary>
        public static ObservableCollection<ChatMessage> LocalChatMessages { get => localChatMessages; set => localChatMessages = value; }

        #region Room Methods
        /// <summary>
        /// Asks the server to return the available lobbies for the user to connect.
        /// </summary>
        /// <returns></returns>
        public static int CreateRoom()
        {
            GUIDSender token = new GUIDSender
            {
                GuidAddress = TCPConnection.ActiveSessionVar.SessionGuid,
                ObjectType = StudentOnlineObject.Type.ROOMADDING
            };
            try
            {
                return TCPConnection.SendSessionRequest(token);
            }
            catch
            {
                return -3;
            }
        }

        public static int SendText(string text, string associativeName)
        {
            ChatMessage newMessage = new ChatMessage
            {
                UserGuid = TCPConnection.ActiveSessionVar.SessionGuid,
                Text = text,
                ConnectedLobby = localLobbies.Where(x => x.AssociativeName == associativeName).First(),
                messageType = ChatMessage.MessageType.TEXTMESSAGE,
                ObjectType = StudentOnlineObject.Type.TEXTSENDING
            };

            try
            {
                return TCPConnection.SendSessionRequest(newMessage);
            }
            catch
            {
                return -4;
            }
        }

        public static int JoinRoom(Lobby lobbyConnection)
        {
            ChatMessage newMessage = new ChatMessage
            {
                UserGuid = TCPConnection.ActiveSessionVar.SessionGuid,
                ConnectedLobby = lobbyConnection,
                messageType = ChatMessage.MessageType.NOTIFICATION,
                ObjectType = StudentOnlineObject.Type.MESSAGESENDING
            };

            try
            {
                return TCPConnection.SendSessionRequest(newMessage);
            }
            catch
            {
                return -4;
            }
        }
        #endregion
    }
}
