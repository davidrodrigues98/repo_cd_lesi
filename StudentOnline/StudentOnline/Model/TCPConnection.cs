﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;

namespace StudentOnline.Model
{
    public static class TCPConnection
    {
        #region Connection Properties
        /// <summary>
        /// This is a simple struct to help the client at keeping track to the server.
        /// </summary>
        public struct ActiveSession
        {
            public Guid SessionGuid;
            /// <summary>
            /// The TCP with the client session will be saved here, since we can't loose it.
            /// </summary>
            public TcpClient sessionTCPClient;
            public Thread sessionActiveThread;
            public bool isAdmin;
        }

        private static ActiveSession activeSession;

        public static ActiveSession ActiveSessionVar { get => activeSession; }

        /// <summary>
        /// Since we are just playing locally, it doesn't really matter changing the IP address for now.
        /// </summary>
        private const string ipAddressStr = "127.0.0.1";
        #endregion

        #region Server Connections

        #region Out of session Methods
        /// <summary>
        /// This is the main function for socket management and communication. Changing this will not affect any classes inside the application.
        /// </summary>
        public static object SendMessage(StudentOnlineObject message)
        {
            TcpClient client = new TcpClient();
            client.Connect(IPAddress.Parse(ipAddressStr), 9000);

            var networkStream = client.GetStream();

            if (networkStream.CanWrite)
            {
                (new BinaryFormatter()).Serialize(networkStream, message);
            }
            while (true)
            {
                if (networkStream.DataAvailable)
                {
                    object receivedPackage = (new BinaryFormatter()).Deserialize(networkStream);
                    // When the response is a Guid we will understand that it is related with a session link.
                    if (receivedPackage is GUIDSender)
                    {
                        ActivateSession((receivedPackage as GUIDSender).GuidAddress, client, (receivedPackage as GUIDSender).IsAdmin);
                        Console.WriteLine("Received login session.");
                    }
                    else
                        client.Close();

                    return receivedPackage;
                }
            }
        }
        #endregion

        #region In-Session Methods
        /// <summary>
        /// This will create a thread to handle constant information that will be transported from server to client and vice versa. This can be real time chat updates, real time message update, etc.
        /// </summary>
        /// <param name="receivedPackage"></param>
        /// <param name="client"></param>
        static void ActivateSession(Guid receivedPackage, TcpClient client, bool admin)
        {
            activeSession = new ActiveSession
            {
                SessionGuid = receivedPackage,
                sessionTCPClient = client,
                sessionActiveThread = new Thread(new ParameterizedThreadStart(SessionPackagesHandler)),
                isAdmin = admin
            };
            activeSession.sessionActiveThread.IsBackground = true;
            activeSession.sessionActiveThread.Start();
        }

        static void SessionPackagesHandler(object data)
        {
            NetworkStream networkStream = activeSession.sessionTCPClient.GetStream();
            while (true)
            {
                Thread.Sleep(100);
                // From now on, this thread will trigger events to stay attached with the main program thread.
                if (networkStream.DataAvailable)
                {
                    object receivedData = (new BinaryFormatter()).Deserialize(networkStream);
                    ObjectProcess(receivedData as StudentOnlineObject);
                }
            }
        }

        static void ObjectProcess(StudentOnlineObject data)
        {
            switch (data.ObjectType)
            {
                case StudentOnlineObject.Type.ROOMADDED:
                    LobbyModel.LocalLobbies.Add(data as Lobby);
                    Console.WriteLine("Added lobby to list.");
                    break;
                case StudentOnlineObject.Type.MESSAGESENT:
                    LobbyModel.LocalChatMessages.Add(data as ChatMessage);
                    Console.WriteLine("New chat notification.");
                    break;
                case StudentOnlineObject.Type.TEXTSENT:
                    LobbyModel.LocalChatMessages.Add(data as ChatMessage);
                    Console.WriteLine("New chat notification.");
                    break;
            }
        }

        /// <summary>
        /// For session but not constant requests.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static int SendSessionRequest(StudentOnlineObject token, bool waitReturn = true)
        {
            //activeSession.sessionActiveThread.Suspend();
            var networkStream = ActiveSessionVar.sessionTCPClient.GetStream();

            if (networkStream.CanWrite)
            {
                (new BinaryFormatter()).Serialize(networkStream, token);
            }

            return 0;
        }
        #endregion

        #endregion
    }
}
