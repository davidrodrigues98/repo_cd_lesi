﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;
using StudentOnline.Model;

namespace StudentOnline
{
    /// <summary>
    /// This controller class will manage client connections with the server when it concerns chat management and permission.
    /// </summary>
    public static class LobbyController
    {
        public static int CreateRoom()
        {
            LobbyModel.CreateRoom();
            return 0;
        }
    }
}
