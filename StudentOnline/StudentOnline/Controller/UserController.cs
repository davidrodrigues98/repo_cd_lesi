﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentOnline.BusinessObject;
using StudentOnline.Model;

namespace StudentOnline.Controller
{
    /// <summary>
    /// Controller for all that concerns users. Throws exceptions when user related actions and/or parses information from client to server.
    /// </summary>
    public static class UserController
    {
        /// <summary>
        /// Validates de user registration fields.
        /// </summary>
        /// <param name="userName">The first and last names.</param>
        /// <param name="userEmail">User company email.</param>
        /// <param name="userPassword">User Password</param>
        /// <param name="userRole">User selected role.</param>
        /// <param name="userCourse">User selected course.</param>
        /// <param name="userCourseUnit">User selected course unit.</param>
        /// <returns></returns>
        public static int UserRegistration(string userName, string userEmail, string userPassword, Role userRole, Course userCourse, CourseUnit userCourseUnit)
        {
            //Checking basic fields.
            if (userName == "" || userEmail == "" || userPassword == "")
                throw new Exception("You must fill all the inputs before submitting.");
            if (userName.Length < 5 || userName.Length > 30)
                throw new Exception("Your name mut be between 5 and 30 characters.");

            //Checking email.
            bool isEmailValid;
            try
            {
                var addr = new System.Net.Mail.MailAddress(userEmail);
                isEmailValid = addr.Address == userEmail;
            }
            catch
            {
                isEmailValid = false;
            }
            if (!isEmailValid)
                throw new Exception("Your email address is not valid.");

            User newUser = new User
            {
                Name = userName,
                Emailaddress = userEmail,
                CourseId = userCourse.Id,
                CourseUnitId = userCourseUnit.Id
            };

            return UserModel.SendRegistrationData(newUser);
        }

        public static object UserLogin(string userEmail, string userPassword)
        {
            //Checking on basic fields.
            if (userEmail == "" || userPassword == "")
                throw new Exception("You must fill all the inputs before submitting.");

            //Checking on email.
            bool isEmailValid;
            try
            {
                var addr = new System.Net.Mail.MailAddress(userEmail);
                isEmailValid = addr.Address == userEmail;
            }
            catch
            {
                isEmailValid = false;
            }
            if (!isEmailValid)
                throw new Exception("Your email address is not valid.");

            LoginCredSender loginAttempt = new LoginCredSender(
                userEmail,
                userPassword
            );

            return UserModel.SendLoginSession(loginAttempt);
        }
    }
}
