﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StudentOnline.Model;
using StudentOnline.Controller;
using StudentOnline.BusinessObject;
using StudentOnline.ViewModel;

namespace StudentOnline
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// All the errors or success codes that our application detects are listed in this dictionary. 
        /// </summary>
        private Dictionary<int, string> returnTranslator = new Dictionary<int, string>
        {
            { -5, "Unable to send your message. Please try again later." },
            { -4, "Unable to connect to the lobby chat." },
            { -3, "Server could not process your request. Please try again." },
            { -2, "Login failed. Please verify your fields and try again."},
            { -1, "The application was unable to connect the server. Please restart or try again later."},        // >0: Server internal errors.
            { 100, "You registered your account. Proceed to the login form and fill it with your credentials." }, //100 - 200: User validation field messages.
            { 101, "An username with this email already exists." }
        };

        public MainWindow()
        {
            InitializeComponent();

            // Tries to connect to server and shuts down if it fails.
            // That's because we need the Role and Course info for registrations.
            // A solution for this could be running this code when the user access the registration window.
            if (UserModel.StartData() == -1)
            {
                MessageBox.Show(returnTranslator[-1]);
                Application.Current.Shutdown();
                return;
            }

            registration_userRole.ItemsSource = UserModel.AvailableRoles;
            registration_userCourse.ItemsSource = UserModel.AvailableCourses;

            // Creates events to link the (not yet) logged in user with the lobbies and messages that server will retrieve for its clients.
            // Note that an observable collection is a collection generic related class, which links these with events that detect whenever an item is added/removed/replaced/etc.
            // This is useful because we can deploy user controls on a very straight forward way.
            LobbyModel.LocalLobbies.CollectionChanged += this.OnLobbyCollectionChanged;
            LobbyModel.LocalChatMessages.CollectionChanged += this.OnMessageReceived;
        }

        #region Deploy User Controls
        void CreateNewLobbyUserControl(Lobby linkedLobby)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ChatSelect newUserControl = new ChatSelect(linkedLobby);
                lobby_chatList.Children.Add(newUserControl);
                newUserControl.MouseDoubleClick += OnDoubleClickChatSelect;

            }));
        }

        void CreateChatTextObject(ChatMessage chatMessage)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if(chatMessage.messageType == ChatMessage.MessageType.TEXTMESSAGE)
                {
                    ChatText newUserControl = new ChatText(chatMessage);
                    lobby_activechat.Children.Add(newUserControl);
                    lobby_sendMessage.Clear();
                }
                else
                {
                    ChatNotification newUserControl = new ChatNotification(chatMessage);
                    lobby_activechat.Children.Add(newUserControl);
                }
            }));
        }

        #endregion

        #region View Events
        /// <summary>
        /// When user double clicks on a lobby selector.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnDoubleClickChatSelect(object sender, MouseButtonEventArgs e)
        {
            int code = LobbyModel.JoinRoom((sender as ChatSelect).LinkedLobby);
            if (code != 0)
                MessageBox.Show(returnTranslator[code]);
            else
            {
                lobby_sendMessage.IsEnabled = true;
                lobby_labelTitle.Content = (sender as ChatSelect).LinkedLobby.AssociativeName;
            }
        }

        /// <summary>
        /// Whenever an user clicks enter to send a message to any room.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int code = LobbyModel.SendText(lobby_sendMessage.Text, lobby_labelTitle.Content.ToString());
                if (code != 0)
                    MessageBox.Show(returnTranslator[code]);
                else
                {
                    lobby_sendMessage.Clear();
                }
            }

        }

        void OnMessageReceived(object sednder, NotifyCollectionChangedEventArgs e)
        {

            NotifyCollectionChangedAction action = e.Action;

            // This is what we want.
            if (action == NotifyCollectionChangedAction.Add)
            {
                List<BusinessObject.ChatMessage> editedOrRemovedItems = new List<BusinessObject.ChatMessage>();
                foreach (BusinessObject.ChatMessage newItem in e.NewItems)
                {
                    editedOrRemovedItems.Add(newItem);
                }

                if (editedOrRemovedItems.Count == 1)
                {
                    CreateChatTextObject(editedOrRemovedItems.First());
                }
            }
        }

        void OnLobbyCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //Get the action which raised the collection changed event
            NotifyCollectionChangedAction action = e.Action;

            // This is what we want.
            if (action == NotifyCollectionChangedAction.Add)
            {
                List<Lobby> editedOrRemovedItems = new List<Lobby>();
                foreach (Lobby newItem in e.NewItems)
                {
                    editedOrRemovedItems.Add(newItem);
                }

                if (editedOrRemovedItems.Count == 1)
                {
                    CreateNewLobbyUserControl(editedOrRemovedItems.First());
                }
            }

            // We are not using this. We will not implement object removals.
            //foreach (string oldItem in e.OldItems)
            //{
            //    editedOrRemovedItems.Add(oldItem);
            //}
        }


        /// <summary>
        /// Whenever the Course selection is changed, the CUs must be updated because not all CUs make part of all courses.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Registration_userCourse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            registration_userCourseUnit.IsEnabled = true;
            IEnumerable<CourseUnit> linkedCourseUnits = UserModel.AvailableCourseUnits.Where(x => x.JoinedCourses.Contains((registration_userCourse.SelectedValue as Course).Id));
            registration_userCourseUnit.ItemsSource = linkedCourseUnits;
        }

        /// <summary>
        /// Whenever an user tries to submit the registration form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Registration_buttonCreateAccount_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Combobox validation.
                if (registration_userRole.SelectedValue == null ||
                    registration_userCourse.SelectedValue == null ||
                        registration_userCourseUnit.SelectedValue == null)
                    throw new Exception("You must select all the options before submitting.");

                //Field Validation.
                int returnCode = UserController.UserRegistration(
                    registration_textboxName.Text,
                    registration_textboxEmail.Text,
                    registration_textboxPassword.Password,
                    registration_userRole.SelectedValue as Role,
                    registration_userCourse.SelectedValue as Course,
                    registration_userCourseUnit.SelectedValue as CourseUnit
                    );

                //Success.
                MessageBox.Show(returnTranslator[returnCode]);

            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        /// <summary>
        /// Whenever an user tries to login. On the backstage, the application will create a session with the server if succeeded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_buttonSubmitLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Field validation.
                object loginSession = UserController.UserLogin(
                    login_textboxEmail.Text,
                    login_textboxPassword.Password
                    );
                if (loginSession is int)
                    MessageBox.Show(returnTranslator[(int)loginSession]);
                else
                {
                    // It succeeded since we got the Guid. Here, we must prepare the lobby search window.
                    lobby_btnCreateLobby.IsEnabled = TCPConnection.ActiveSessionVar.isAdmin;
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        /// <summary>
        /// Activates when a teacher wants to create a new room.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Lobby_btnCreateLobby_Click(object sender, RoutedEventArgs e)
        {
            int code = LobbyController.CreateRoom();
            if (code != 0)
            {
                MessageBox.Show(returnTranslator[code]);
            }
        }

        #endregion


    }
}
